<script type="text/javascript">

//function that returns characters found in two strings
function find_chars(string1, string2){
   var ans="";
   for (var i = 0; i < string1.length; i++) {
   		 for (var j = 0; j < string2.length; j++) {
           if(string1.charAt(i)==string2.charAt(j)) ans += string1.charAt(i);
       }
   }
   return ans;
}

//function to compact array
function compact_array(arr){
   for (var i = 0; i < arr.length; i++) {
       var it = arr[i];
       for(var j=i+1; j<arr.length; j++){
          if(it==arr[j]){
             arr.splice(j--, 1);
          }
       }
   }
   return arr;
}

//function to rotate array
function rotate_array(arr, n){
   for (var i = 0; i < n; i++) {
       var item=arr.pop();
       arr.unshift(item);
   }
   return arr;
}

//function that returns the lcm of an array of integers
function get_array_lcm(arr){
   var res=arr[0];
   for(i=1; i<arr.length; i++){
   	  res = get_lcm(res, arr[i]);
   }
   return res;
}
function get_lcm(a, b){
    return (a * b / get_gcd(a, b));
}
function get_gcd(x, y){
   var z;
   while (y != 0){
      z = y;
      y = x % y;
      x = z;
  }
  return x;
}
</script>