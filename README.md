# README #

This repository contains my solution to the programming test given by eHealth Africa.

* tree_traversal.txt contains the answer to the first question.

* myfile.js contains the answers to questions 2 to 5.

* I am not able to submit the answer to question 6 because I need more time to work on it and I do not want to miss the deadline.

Thanks